
################################################################################
#! Building docker base image from the app ROOT folder
#docker build -f Dockerfile -t iprimo/app_base .   #(run this from app_base folder)
#docker build -f app_base/Dockerfile -t iprimo/app_base .


################################################################################
#!
docker build -f ./app/docker/dev/Dockerfile -t iprimo/app_dev:lastest .


################################################################################
### Compose app - test
# docker-compose -f ./docker-compose_test.yml build
# docker-compose -f ./docker-compose_test.yml up -d

docker-compose -f ./app/docker/dev/docker-compose_test.yml up -d --build


################################################################################
### Compose app
# docker-compose -f ./docker-compose.yml build
# docker-compose -f ./docker-compose.yml up -d

docker-compose -f ./app/docker/dev/docker-compose.yml up -d --build


################################################################################
### helpful commands
time docker run --rm app_dev


